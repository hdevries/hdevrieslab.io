---
title: "Portfolio"
# description: "Software and security engineer"
type: page
menu: "main"
# omit_header_text: true
---


# CV


<https://www.linkedin.com/in/hielkedv/>


# Other portfolio

[A blog about non-blocking IO](https://medium.com/ing-blog/how-does-non-blocking-io-work-under-the-hood-6299d2953c74)

[A talk about how reverse engineering of binaries works (Libreoffice file)](../doc/how_software_is_cracked.odp)

[Some things I did for IO_uring](https://twitter.com/hielkedv)

[A presentation about writing readable code (Keynote file)](../doc/readable_code_presentation.key)

[Some security trainings I built](https://github.com/frevib/learnxss)

[Advent of Code 2019 in Haskell](https://github.com/frevib/advent-of-code-2019)

[Github](https://github.com/frevib)

# Latest experience

Currently I am a Scala engineer at DHL Parcel. The product we build consists of several microservices that work together to handle millions of messages per day. The stack includes:

* Scala with ZIO
* Doobie
* Tapir
* Kafka
* K8s

----

Further CV: <https://www.linkedin.com/in/hielkedv/>